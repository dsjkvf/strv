strv
====

## About

`strv`, which stands for *'Stream with VLC'* (or, may be, for [*'sterva'*](https://en.wiktionary.org/wiki/%D1%81%D1%82%D0%B5%D1%80%D0%B2%D0%B0)) is a shell script for streaming media with the help of VLC via its [Remote Control](https://wiki.videolan.org/RC_Interface/#rc) interface.

## Usage

`strv` takes one and only one argument, either the path (quoted or escaped if needed) to the media to be streamed, or the switch `-q`, which will tell `strv` to find and to stop all the currently active streams.

Examples:

    strv 00.m3u
    strv '/Users/Me/Desktop/My Album/'
    strv /Users/Me/Desktop/My\ Recent\ Video.mp4
    strv -q

However, `strv` can also parse [pyradio's](https://github.com/coderholic/pyradio) or VLC's media libraries. In order to do that, check the 'OPTIONS' section of the script, update the variables if necessary, and then launch `strv` without any arguments: in such case, `strv` with present you with a list of sources found in the corresponding library (depending on the values set in the script), and upon your choice, will start streaming the selected media.

For more information on the commands available in VLC's Remote Control, you can refer to this [post](https://n0tablog.wordpress.com/2009/02/09/controlling-vlc-via-rc-remote-control-interface-using-a-unix-domain-socket-and-no-programming/), or simply run `help` in its command line.

## System requirements

In most cases, `strv` will work just fine with the older Bash versions -- however Bash 4.0 and above is recommended.
